
#include <FS.h>
#include <Arduino.h>
#include <Hash.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ModbusMaster.h>
#include <SimpleTimer.h>
#include <ESPAsyncWiFiManager.h> 

#define LED 2  //On board LED
#define WIFI_LED D0
#define COM_LED D1
#define N_POINTS 480
#define RS485_PERIOD_MS 500
#define DIVIDER_HIST ((24*60*1000)/N_POINTS/(RS485_PERIOD_MS))*60


uint16_t count = DIVIDER_HIST;
const char *header = "HTTP/1.1 200 OK\r\n"
    "Content-Type: text/xml\r\n"
    "Access-Control-Allow-Origin: *\r\n"
    "Accept-Ranges: none\r\n"
    "Transfer-Encoding: chunked\r\n"
    "Connection: close\r\n"
    "\r\n";

struct pvData{
  float voltage[N_POINTS];
  float current[N_POINTS];
  float power[N_POINTS];
};
struct btData{
  float current[N_POINTS];
  float voltage[N_POINTS];
  float temp[N_POINTS];
};
struct ldData{
  float current[N_POINTS];
  float voltage[N_POINTS];
  float power[N_POINTS];
};
struct powerData{
  pvData pv;
  btData bt;
  ldData ld;
  unsigned long time[N_POINTS];
  bool valid[N_POINTS] = {};
};

struct genData{
  float genToday;
  float genMonth;
  float genYear;
  float genTotal;
};

enum State { XML_Preamble, XML_Body, XML_Done };

AsyncWebServer server(80);
DNSServer dns;


ModbusMaster node;
SimpleTimer timer;


struct powerData p_data;
struct genData gen_data;

int timerTask1, timerTask2, timerTask3;

uint16_t index_counter = 0;
bool rs485Busy = false;
uint8_t result;

bool read_register_3100() {
  digitalWrite(COM_LED, 1);
  result = node.readInputRegisters(0x3100, 7);
  digitalWrite(COM_LED, 0);
  if (result != node.ku8MBSuccess) 
  {
    return false;
  } 
  p_data.pv.voltage[index_counter] = (long)node.getResponseBuffer(0x00) / 100.0f;
  p_data.pv.current[index_counter] = (long)node.getResponseBuffer(0x01) / 100.0f;
  p_data.pv.power[index_counter] = ((long)node.getResponseBuffer(0x03) << 16 | node.getResponseBuffer(0x02)) / 100.0f;
  p_data.bt.voltage[index_counter] = node.getResponseBuffer(0x04) / 100.0f;
  p_data.bt.current[index_counter] = (long)node.getResponseBuffer(0x05) / 100.0f;
  p_data.ld.current[index_counter] = (long)node.getResponseBuffer(0x0D) / 100.0f;
  p_data.ld.power[index_counter] = ((long)node.getResponseBuffer(0x0F) << 16 | node.getResponseBuffer(0x0E)) / 100.0f;
  p_data.bt.temp[index_counter] = node.getResponseBuffer(0x11) / 100.0f;

  
  return true;
}


bool read_register_3300() {
  digitalWrite(COM_LED, 1);
  result = node.readInputRegisters(0x3300, 0x13);
  digitalWrite(COM_LED, 0);
  if (result != node.ku8MBSuccess) 
  {
    return false;
  } 

  gen_data.genToday = ((long)node.getResponseBuffer(0x0D) << 16 | node.getResponseBuffer(0x0C)) / 100.0f;
  gen_data.genMonth = ((long)node.getResponseBuffer(0x0F) << 16 | node.getResponseBuffer(0x0E)) / 100.0f;
  gen_data.genYear = ((long)node.getResponseBuffer(0x11) << 16 | node.getResponseBuffer(0x10)) / 100.0f;
  gen_data.genTotal = ((long)node.getResponseBuffer(0x13) << 16 | node.getResponseBuffer(0x12)) / 100.0f;
  return true;
}

void read_from_tracer()
{
  if(rs485Busy) return;
  rs485Busy = true;
  count++;

  if(count >= DIVIDER_HIST){
    //read success. Increment array index.

    index_counter++;
    count = 0;
  }

  if(index_counter >= N_POINTS)
  {
    index_counter = 0;
  }
  if(!read_register_3300())
  {
    //failed to read. Exit
    digitalWrite(LED,!digitalRead(LED));
    rs485Busy = false;
    return;
  }
  if(!read_register_3100())
  {
    //failed to read. Exit
    digitalWrite(LED,!digitalRead(LED));
    rs485Busy = false;
    return;
  }

  p_data.time[index_counter] = millis();
  p_data.valid[index_counter] = true;



  digitalWrite(LED,1);
  rs485Busy = false;
}

void XML_hist_data(AsyncWebServerRequest *request)
{
  int* lambda_copy_ix = new int(index_counter+2);
  int* lambda_counter = new int(0); 
  int* lambda_state = new int(0); 
  AsyncWebServerResponse *response = request->beginChunkedResponse("text/xml",  [lambda_counter, lambda_state, lambda_copy_ix](uint8_t *buffer, size_t maxLen, size_t index) mutable -> size_t  {
    String temp = "";


    if(*lambda_state == XML_Done)
    {
    delete lambda_counter;
    delete lambda_state;
    delete lambda_copy_ix;
    return 0;
    }

    if(*lambda_state == XML_Body)
    {
      temp = "";
      for(uint32_t iteration = 0; iteration < 5 || temp.length() == 0; iteration++)
      {
        uint32_t index = (*lambda_copy_ix+*lambda_counter)%N_POINTS;
        if(p_data.valid[index])
        {
          temp.concat("<d_p t=\""+String(p_data.time[index])+"\">");

          temp.concat("<pv_p>");
          temp.concat(String(p_data.pv.power[index]));
          temp.concat("</pv_p>");

          temp.concat("<pv_c>");
          temp.concat(String(p_data.pv.current[index]));
          temp.concat("</pv_c>");

          temp.concat("<pv_v>");
          temp.concat(String(p_data.pv.voltage[index]));
          temp.concat("</pv_v>");

          temp.concat("<b_v>");
          temp.concat(String(p_data.bt.voltage[index]));
          temp.concat("</b_v>");

          temp.concat("<b_c>");
          temp.concat(String(p_data.bt.current[index]));
          temp.concat("</b_c>");

          temp.concat("</d_p>");
        }

        if(*lambda_counter > N_POINTS-5)
        {
          *lambda_state = XML_Done;
          temp.concat("</p_data>");
          iteration = 5;

        }
        else
        {
          *lambda_counter = *lambda_counter + 1;
        }
      }
    }

    if(*lambda_state == XML_Preamble)
    {
      temp = "";
      temp.concat("<?xml version = \"1.0\" ?>");
      temp.concat("<p_data>");
      temp.concat("<curr_time>");
      temp.concat(String(millis()));
      temp.concat("</curr_time>");
      *lambda_state = XML_Body;
    }
    strncpy((char *)buffer, temp.c_str(), maxLen);
    return min(maxLen, temp.length());
  });
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}


void XML_curr_data(AsyncWebServerRequest *request)
{
  AsyncResponseStream *response = request->beginResponseStream("text/xml");
  response->addHeader("Access-Control-Allow-Origin", "*");
  uint16_t ix = index_counter;
  
  response->print("<?xml version = \"1.0\" ?>");
  response->print("<p_data>");

  if(p_data.valid[ix])
  {
    response->print("<d_p t=\""+String(p_data.time[ix])+"\">");
    
    response->print("<pv_v>");
    response->print(String(p_data.pv.voltage[ix]));
    response->print("</pv_v>");

    response->print("<pv_c>");
    response->print(String(p_data.pv.current[ix]));
    response->print("</pv_c>");
    
    response->print("<pv_p>");
    response->print(String(p_data.pv.power[ix]));
    response->print("</pv_p>");

    response->print("<b_c>");
    response->print(String(p_data.bt.current[ix]));
    response->print("</b_c>");

    response->print("<b_v>");
    response->print(String(p_data.bt.voltage[ix]));
    response->print("</b_v>");

    response->print("</d_p>");
  }
  response->print("</p_data>");

  request->send(response);
}

void XML_gen_data(AsyncWebServerRequest *request)
{

  AsyncResponseStream *response = request->beginResponseStream("text/xml");
  response->addHeader("Access-Control-Allow-Origin", "*");
  
  response->print("<?xml version = \"1.0\" ?>");
  response->print("<g_data>");
  response->print("<gt>");
  response->print(String(gen_data.genToday));
  response->print("</gt>");
  response->print("<gm>");
  response->print(String(gen_data.genMonth));
  response->print("</gm>");
  response->print("<gy>");
  response->print(String(gen_data.genYear));
  response->print("</gy>");
  response->print("<gtot>");
  response->print(String(gen_data.genTotal));
  response->print("</gtot>");
  response->print("</g_data>");

  request->send(response);
}

void simulate_data()
{
  digitalWrite(COM_LED, 1);
  count++;

  if(count >= DIVIDER_HIST){
    //read success. Increment array index.

    index_counter++;
    count = 0;
  }
  if(index_counter >= N_POINTS)
  {
    index_counter = 0;
  }

  p_data.time[index_counter] = millis();
  p_data.pv.voltage[index_counter] = float(500+random(-20, 20))/10;
  p_data.pv.current[index_counter] = float(70+random(-30,30))/10;
  p_data.pv.power[index_counter] = float(350+random(-50,50))/10;
  p_data.bt.voltage[index_counter] = float(130+random(-10,10))/10;
  p_data.bt.current[index_counter] = float(250+random(-50,50))/10;

  p_data.valid[(index_counter+1)%N_POINTS] = false;
  p_data.valid[(index_counter+2)%N_POINTS] = false;
  p_data.valid[(index_counter+3)%N_POINTS] = false;
  p_data.valid[(index_counter+4)%N_POINTS] = false;
  p_data.valid[index_counter] = true;


  gen_data.genToday = float(10+random(0, 20))/10;
  gen_data.genMonth = float(100+random(0, 20))/10;
  gen_data.genYear = float(1000+random(0, 20))/10;
  gen_data.genTotal = float(10000+random(0, 20))/10;

  digitalWrite(COM_LED, 0);

}

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}


void setup() {

  //Onboard LED port Direction output
  pinMode(LED,OUTPUT); 
  pinMode(WIFI_LED,OUTPUT); 
  pinMode(COM_LED,OUTPUT); 
  
  Serial.begin(115200);
  AsyncWiFiManager wifiManager(&server,&dns);
  //wifiManager.resetSettings();
  wifiManager.setBreakAfterConfig(true);
  digitalWrite(WIFI_LED,0);
  wifiManager.autoConnect("SOLAR");
  node.begin(1, Serial);

  SPIFFS.begin();
  Serial.println("");




  String str = "";
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
      str += dir.fileName();
      str += " / ";
      str += dir.fileSize();
      str += "\r\n";
  }
  Serial.print(str);
 

  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  //Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
  
  digitalWrite(WIFI_LED,1);

  server.on("/curr_data.xml", HTTP_GET, [](AsyncWebServerRequest *request){
      digitalWrite(WIFI_LED, 0);
      XML_curr_data(request);
      digitalWrite(WIFI_LED, 1);
    });
  server.on("/hist_data.xml", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(WIFI_LED, 0);
    XML_hist_data(request);
    digitalWrite(WIFI_LED, 1);
  });

  server.on("/gen_data.xml", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(WIFI_LED, 0);
    XML_gen_data(request);
    digitalWrite(WIFI_LED, 1);
  });

  server.serveStatic("/www/", SPIFFS, "/www/");
  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

  server.onNotFound(notFound);
  server.begin();                  //Start server
  
  Serial.println("HTTP server started");

  timerTask2 = timer.setInterval(RS485_PERIOD_MS, read_from_tracer);

  //for(uint32_t ix = 0; ix < 1000; ix++)
  //{
  //  simulate_data();
  //}
  //timerTask3 = timer.setInterval(RS485_PERIOD_MS, simulate_data);
  
}

void loop() {
  timer.run();
}